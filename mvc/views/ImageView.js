var ImageMenuView = ToolbarView.extend({
	events:{
		'tap .main-toolbar-item.src':'changeSrc'
	},
	render_:function(){
		var self = this;
		var menu_tpl = [
			'<ul class="main-toolbar-wrapper">',
				'<li class="main-toolbar-item src">更换</li>',
				this.commonTpl,
			'</ul>',
		].join('\n');
		this.$el.html(menu_tpl);
		return this.$el;
	},
	resizeImage:function(){
		var target = ctpl.get('target');
		var width = target.width() * 0.8;
		var img = target.find('img');
		if(img.length==1){
			img.width = width;
		}
	},
	changeSrc:function(e){
		var self = this;
		var target = ctpl.get('target');
		var view = ctpl.get('view');
		var parent = view.opts.parent;
		var img = target.find('img');
		var src = prompt('图片地址：');
		if(src && src.trim().length>0){
			var image = new Image();
			parent.toggleLoadingLayout();
			image.onload = function(){
				var width = image.width>=target.width()?'80%':image.width;
				if(img.length==1){
					img[0].src = src;
					img.css({width:width})
				}else{
					target.prepend(image);
					$(image).css({width:width});
				}
				parent.toggleLoadingLayout(true);
			};
			image.src = src;
		}
	},
	doSelectActions:function(e,select){
		if(select.is('.title')){
		}
	}
});
var ImageView = TemplateView.extend({
	classPrefix:'ctpl-image',
	events:{
		'rotate [class^="ctpl-image"] img':'rotateImage'
	},
	updateMenu:function(){
		this.menu = new ImageMenuView(this.opts);	
	},
	rotateImage:function(e){
		var g = e.gesture;
		var target = $(e.target);
		/*var ort = target.data('rotate');
		var timestamp = target.data('timestamp');
		if(!ort || !timestamp){
			ort = 0;
			timestamp = new Date().getTime();
		}
		var temp = g.rotation;
		*/
		var rt = g.rotation;
		/*
		var now = new Date().getTime();
		if(now - timestamp > 10){
			//rt+=ort;
		}
		*/
		if(Math.abs(rt)<=90){
			target.css({
				'-webkit-transform':'rotate('+rt+'deg)'
			});
			/*
			target.data('rotate',temp);
			target.data('timestamp',new Date().getTime())
			*/
		}
		event.preventDefault();
	}
});