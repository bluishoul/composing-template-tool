var CatalogMenuView = ToolbarView.extend({
	render_:function(){
		var self = this;
		var menu_tpl = [
			'<ul class="main-toolbar-wrapper">',
				'<li class="main-toolbar-item refresh">更新</li>',
				this.commonTpl,
			'</ul>',
		].join('\n');
		this.$el.html(menu_tpl);
		return this.$el;
	},
	events:{
		'tap .main-toolbar-item.refresh':'refreshCatalog'
	},
	refreshCatalog:function(main){
		var titles = $('[class^="ctpl-title"]');
		var target = ctpl.get('target');
		if(target)
			target = target.find('.catalog-content');
		else
			target = main.find('.catalog-content');
		var ul = $('<ul></ul>');
		for(var i=0;i<titles.length;i++){
			var title = titles[i];
			var className = title.className.replace('ctpl-title-h','catalog-item-type');
			var tp = title.className.match(/ctpl-title-h([1-3])*/ig);
			if(tp && tp.length!=0){
				tp = tp[0];
				tp = +(tp.match(/[1-3]/)[0]);
			}
			var id = 'h'+tp+''+i;
			title.id = id;
			var a = $('<a></a>').attr('href','#'+id).html(title.innerText).append('<span></span>');
			var li = $('<li></li>').addClass(className).append(a);
			ul.append(li);
		}
		target.html(ul);
	},
	findTitle:function(titles,type,ul){
		var old_type = type;
		var self = this;
		for(var i=0;i<titles.length;i++){
			var title = titles[i];
			var tp = title.className.match(/ctpl-title-h([1-3])*/ig);
			if(tp && tp.length!=0){
				tp = tp[0];
				tp = +(tp.match(/[1-3]/)[0]);
			}
			if(type == tp){
				var text = title.innerText;
				var li = $('<li></li>').html(text);
				ul.append(li);
				var new_titles = titles.not('eq('+i+')');
				self.findTitle(new_titles,type+1,li);
			}
		}

	}
});
var CatalogView = TemplateView.extend({
	classPrefix:'ctpl-catalog',
	editable:false,
	events:{
		'tap .catalog-head':'toggleCatalog'
	},
	updateMenu:function(){
		this.menu = new CatalogMenuView(this.opts);	
		this.menu.refreshCatalog(this.main);
		var target = $(this.main);
		target.toggleClass('on off');
		target.siblings('.catalog-content').toggleClass('on off');
	},
	toggleCatalog:function(e){
		var target = $(e.target);
		target.toggleClass('on off');
		target.siblings('.catalog-content').toggleClass('on off');
	}
});